import { Component, OnInit } from '@angular/core';
import { VideoserviceService } from "../../services/video/videoservice.service";
import { ActivatedRoute, Router } from "@angular/router";
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-parentsmain',
  templateUrl: './parentsmain.component.html',
  styleUrls: ['./parentsmain.component.css']
})
export class ParentsmainComponent implements OnInit {
  token = localStorage.getItem('token');
  videos : any = [];
  error: string = "";
  showerror : boolean = false;
  url: string = "";

  constructor(private videoservice: VideoserviceService, private router: Router) { }

  ngOnInit() {
    this.checkSession();
    this.getVideos();
  }

  //Gets all the videos
  getVideos(){
    var decodetoken =  jwt_decode(this.token);
    this.videoservice.getVideos(decodetoken['userId']).subscribe(
      res => {
        console.log(res);
        this.videos = res;
      },
      err => console.log(err)
    );

  }
//Deletes a video
  DeleteVideo(id :any){
    this.videoservice.deleteVideo(id)
    .subscribe(
      res=>{
        console.log(res);
        if(res != null){
          this.getVideos();
        }

      },
      (err) =>  {
        if(err['status'] == 401){
          this.showerror = true;
          this.error = err['error']['message'];
        }
        if(err['status'] == 404){
          this.showerror = true;
          this.error = err['error']['message'];
        }
        if(err['status'] == 500){
          this.showerror = true;
          this.error = err['error']['message'];
        }
      }
    )
  }

  //Shows a video
  ShowVideo(u :any){
    this.url = u;
  }

  //Checks if the token is valid - Guard
  checkSession(){
    var actualDate = new Date();
    var token = localStorage.getItem('token');
    console.log(token);
    if(token != null){
      var decodetoken =  jwt_decode(token);
      console.log(decodetoken);
      if(decodetoken['permission'][0] != 'create' || decodetoken['permission'][1] != 'edit' || decodetoken['permission'][2] != 'delete'){
        localStorage.removeItem('token');
        console.log('get');
        this.router.navigate(['/index']);  
      }
      if(decodetoken['created'] / 3 != actualDate.getHours()){
        console.log('created');
        localStorage.removeItem('token');
        this.router.navigate(['/index']);
      }
      if((decodetoken['expire'] / 5 - actualDate.getHours()) != 1){
        console.log('expire');
        localStorage.removeItem('token');
        this.router.navigate(['/index']);
      }
    }
    else
    {
      this.router.navigate(['/index']);
    }
  
  }


}
