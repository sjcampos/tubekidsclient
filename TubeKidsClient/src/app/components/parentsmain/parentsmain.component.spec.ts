import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentsmainComponent } from './parentsmain.component';

describe('ParentsmainComponent', () => {
  let component: ParentsmainComponent;
  let fixture: ComponentFixture<ParentsmainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentsmainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentsmainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
