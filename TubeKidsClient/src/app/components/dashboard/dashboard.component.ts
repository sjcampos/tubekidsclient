import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  //Logout the user
  logout(){
    
    localStorage.removeItem('token');
    this.router.navigate(['/index']);
  }

  //Checks if the token is valid - Guard
  checkSession(){
    var actualDate = new Date();
    var token = localStorage.getItem('token');
    console.log(token);
    if(token != null){
      var decodetoken =  jwt_decode(token);
      console.log(decodetoken);
      if(decodetoken['permission'][0] != 'create' || decodetoken['permission'][1] != 'edit' || decodetoken['permission'][2] != 'delete'){
        localStorage.removeItem('token');
        console.log('get');
        this.router.navigate(['/index']);  
      }
      if(decodetoken['created'] / 3 != actualDate.getHours()){
        console.log('created');
        localStorage.removeItem('token');
        this.router.navigate(['/index']);
      }
      if((decodetoken['expire'] / 5 - actualDate.getHours()) != 1){
        console.log('expire');
        localStorage.removeItem('token');
        this.router.navigate(['/index']);
      }
    }
    else
    {
      this.router.navigate(['/index']);
    }
  
  }
}
