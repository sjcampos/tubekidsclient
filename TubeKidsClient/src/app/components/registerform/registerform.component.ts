import { Component, OnInit } from '@angular/core';
import { user } from "../../models/user";
import { UserserviceService } from '../../services/user/userservice.service';
import { ActivatedRoute, Router } from "@angular/router";
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { TimeoutError } from 'rxjs';
@Component({
  selector: 'app-registerform',
  templateUrl: './registerform.component.html',
  styleUrls: ['./registerform.component.css']
})
export class RegisterformComponent implements OnInit {

  error: string = "";
  showerror : boolean = false;
  newdate :string ="";
  birthday : Date = new Date;
  newuser : user ={
    id:"",
    name:"",
    last_name:"",
    country : "",
    phone: "",
    birthday :  "",
   email : "",
   password : "",
   repass : ""
  };
  constructor(private userservice: UserserviceService, private router: Router) { }

  ngOnInit() {
  }

  RegisterUser(){
    var tempdate = this.birthday.toString();
    var re = /-/gi;
    this.newdate = tempdate.replace(re,"/") ;
    this.newuser.birthday = this.newdate;
    if(this.ValidData(this.newuser)){
      if(this.ValidData(this.newuser)){
        this.userservice.registerUser(this.newuser)
        .subscribe(
          res=>{
            if(res != null){
              console.log(res);
              this.router.navigate(['/registerverification']);
            }
  
          },
          err =>  {
            
            if(err['status']== 400){
                this.showerror = true;
                this.error = err['error']['message'];
            }
            if(err['status'] == 404){
              this.showerror = true;
              this.error = err['error']['message'];
            }
            if(err['status'] == 500){
              this.showerror = true;
              this.error = err['error']['message'];
            }
          } 
        );
      }
      
    }

  }

  BadData(err : any){
    this.error = err;
    this.showerror = true;
  }

  //Validates the data before sending the request
  ValidData(u : user){
    if(u.name.trim() == "" || u.name == null || u.name == undefined){
      this.error = "You need to fill the name space!!";
      this.showerror = true;
      return false;
    }
    if(u.last_name.trim() == "" || u.last_name == null || u.last_name == undefined){
      this.error = "You need to fill the last name space!!";
      this.showerror = true;
      return false;
    }
    if(u.country.trim() == "" || u.country == null || u.country == undefined){
      this.error = "You need to fill the country space!!";
      this.showerror = true;
      return false;
    }
    if(u.phone.trim() == "" || u.phone == null || u.phone == undefined){
      this.error = "You need to fill the phone space!!";
      this.showerror = true;
      return false;
    }
    if(u.birthday == null || u.birthday == undefined){
      this.error = "You need to fill the birthday space!!";
      this.showerror = true;
      return false;
    }
    if(u.email.trim() == "" || u.email == null || u.email == undefined){
      this.error = "You need to fill the email space!!";
      this.showerror = true;
      return false;
    }
    if(u.password.trim() == "" || u.password == null || u.password == undefined){
      this.error = "You need to fill the password space!!";
      this.showerror = true;
      return false;
    }
    if(u.repass.trim() == "" || u.repass == null || u.repass == undefined){
      this.error = "You need to fill the email space!!";
      this.showerror = true;
      return false;
    }
    if(u.password != u.repass){
      this.error = "The confirm password space and the password does not match!!";
      this.showerror = true;
      return false;
    }
    if(this.ValidAge(u.birthday)){
      this.error = "The person in charge of the account must be of legal age!!";
      this.showerror = true;
      return false;
    }
    if(this.ValidEmail(u.email)){
      this.error = "Enter a valid email!!";
      this.showerror = true;
      return false;
    }
    else{
      return true;
    }

  }

  //Validates the email format
ValidEmail(e :string){
  var patron = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
  if (e.search(patron) == 0) {
      return false;
  } else {
      return true;
  }
}

//Validates the age
ValidAge(d : any){
  
    var values = d.split("/");
    var day = values[2];
    var month = values[1];
    var year = values[0];

    var fecha_hoy = new Date();
    var actual_year = fecha_hoy.getFullYear();
    var actual_month = fecha_hoy.getMonth() + 1;
    var actual_day = fecha_hoy.getDate();

    var age = (actual_year + 1900) - year;
    if (actual_month < month) {
        age--;
    }
    if ((month == actual_month) && (actual_day < day)) {
        age--;
    }
    if (age > 1900) {
        age -= 1900;
    }
    if (age < 18) {
        return true;
    } else {
        return false;
    }
  }

}
