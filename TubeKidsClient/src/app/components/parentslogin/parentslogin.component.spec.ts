import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentsloginComponent } from './parentslogin.component';

describe('ParentsloginComponent', () => {
  let component: ParentsloginComponent;
  let fixture: ComponentFixture<ParentsloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentsloginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentsloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
