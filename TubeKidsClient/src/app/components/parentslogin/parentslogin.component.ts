import { Component, OnInit } from '@angular/core';
import { userlogin } from '../../models/userlogin';
import { UserserviceService } from '../../services/user/userservice.service';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-parentslogin',
  templateUrl: './parentslogin.component.html',
  styleUrls: ['./parentslogin.component.css']
})
export class ParentsloginComponent implements OnInit {

  error: string = "";
  shownoactive : boolean = false; 
  showerror : boolean = false;
  userlogin: userlogin ={
    email:"",
    password:""
  };
  constructor(private userservice: UserserviceService, private router: Router) { }

  ngOnInit() {
  }

  //Validates that is valid user
  AuthData(){
    if(this.ValidData(this.userlogin)){
      this.userservice.checkUSer(this.userlogin)
      .subscribe(
        res=>{
          console.log(res);
          if(res['message'] == '002'){
            
            this.router.navigate(['/smsverification']);
          }

        },
        (err) =>  {
          if(err['status']== 400){
              this.shownoactive = true;
              console.log(err);
          }
          if(err['status'] == 404){
            this.showerror = true;
            this.error = err['error']['message'];
          }
          if(err['status'] == 500){
            this.showerror = true;
            this.error = err['error']['message'];
          }
        }
      );
    }
  }



  //Validates the user data before sending a request
  ValidData(user : userlogin){
    if(user.email == "" || user.password == ""  || user.email == null || user.password == null || user.email == undefined ||
    user.password == undefined || user.email.trim() == "" || user.password.trim() == ""){
      console.log(user.password);
      this.showerror = true;
      this.error = "You need to complete all the fields!!";
      return false;
    }
    if(this.ValidEmail(user.email)){
      this.showerror = true;
      this.error = "You need to enter a valid email!!";
      return false;
    }
    else{
      return true;
    }

  }
//Checks if the email format is valid
  ValidEmail(email : string){
    var patron = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
    if (email.search(patron) == 0) {
        return false;
    } else {
        return true;
    }
  }

}
