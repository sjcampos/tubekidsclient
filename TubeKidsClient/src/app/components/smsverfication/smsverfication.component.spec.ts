import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsverficationComponent } from './smsverfication.component';

describe('SmsverficationComponent', () => {
  let component: SmsverficationComponent;
  let fixture: ComponentFixture<SmsverficationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsverficationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsverficationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
