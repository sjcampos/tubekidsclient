import { Component, OnInit } from '@angular/core';
import { UserserviceService } from '../../services/user/userservice.service';
import { ActivatedRoute, Router } from "@angular/router";
import { sms } from '../../models/sms';

@Component({
  selector: 'app-smsverfication',
  templateUrl: './smsverfication.component.html',
  styleUrls: ['./smsverfication.component.css']
})
export class SmsverficationComponent implements OnInit {

  error: string = "";
  showerror : boolean = false;
  smscode : sms = {
    verificationcode :""
  };
  constructor(private userservice: UserserviceService, private router: Router) { }

  ngOnInit() {
  }

  //Creates a session is two factor is correct
  CreateSession(){
    if(this.ValidCode(this.smscode.verificationcode)){
      this.userservice.checkmessage(this.smscode)
      .subscribe(
        res =>{
          localStorage.setItem('token', res['token']);
          console.log(res);
          this.router.navigate(['/parentsmain']);
        },
        err => this.BadData(err)
      )

    }
    
  }

  BadData(err : any){
    this.showerror = true;
    this.error = err;
  }

  //Validates data
  ValidCode(code: string){
    if(code == null || code.trim() == "" || code == undefined){
      this.error = 'You need to enter a code';
      this.showerror = true;
      return false;
    }
    else{
      return true;
    }
  }

}
