import { Component, OnInit } from '@angular/core';
import { kidprofile } from '../../models/kidprofile';
import { ProfileserviceService} from "../../services/profile/profileservice.service";
import { ActivatedRoute, Router } from "@angular/router";
import * as jwt_decode from 'jwt-decode';
import { newkidprofile } from '../../models/newkidprofile';
import { isNumber } from 'util';

@Component({
  selector: 'app-profileform',
  templateUrl: './profileform.component.html',
  styleUrls: ['./profileform.component.css']
})
export class ProfileformComponent implements OnInit {

  update: boolean = false;
  register: boolean = true;
  error: string = "";
  showerror : boolean = false;
  kidprofile: kidprofile={
    _id:"",
    id_father:"",
    name: "",
    user_name:"",
    pin: "",
    age:""
  };
  newkidprofile: newkidprofile={    
    id: "",
    id_father: "",
    name: "",
    user_name: "",
    pin: "",
    age: ""
  };

  token = localStorage.getItem('token');
  constructor(private profileservice: ProfileserviceService, private router: Router, private activedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.checkSession();
    const params = this.activedRoute.snapshot.params;
    console.log(params);
    if(params.id){
      this.update = true;
      this.register = false;
      this.profileservice.getProfileUpdate(params.id).subscribe(
        res => {
          console.log(res);
          this.kidprofile = res;
          console.log(this.kidprofile._id);
          
        },
        err => console.log(err)
      );
    }
  }

  //Registers a new profiles
  RegisterProfile(){
    var decodetoken =  jwt_decode(this.token); 
    this.kidprofile.id_father = decodetoken['userId'];

      if(this.ValidData(this.kidprofile)){
        this.newkidprofile.id = this.kidprofile._id;
        this.newkidprofile.id_father = this.kidprofile.id_father;
        this.newkidprofile.name = this.kidprofile.name;
        this.newkidprofile.user_name = this.kidprofile.user_name;
        this.newkidprofile.pin = this.kidprofile.pin;
        this.newkidprofile.age = this.kidprofile.age;
        console.log(this.newkidprofile);
        this.profileservice.registerProfile(this.newkidprofile)
        .subscribe(
          res=>{
            if(res != null){
              console.log(res);
              this.router.navigate(['/mainprofile']);
            }
          },
          (err) =>  {
            if(err['status'] == 400){
              this.showerror = true;
              this.error = err['error']['message'];
            }
            if(err['status'] == 401){
              this.showerror = true;
              this.error = err['error']['message'];
            }
            if(err['status'] == 404){
              this.showerror = true;
              this.error = err['error']['message'];
            }
            if(err['status'] == 500){
              this.showerror = true;
              this.error = err['error']['message'];
            }
          }
        )
      }
      
    

  }

  //Updates a profile
  UpdateProfile(){
    console.log(this.kidprofile._id);
      if(this.ValidData(this.kidprofile)){
        this.profileservice.updateProfile(this.kidprofile,this.kidprofile._id)
        .subscribe(
          res=>{
            if(res != null){
              console.log(res);
              this.router.navigate(['/mainprofile']);
            }
  
          },
          (err) =>  {
            if(err['status'] == 401){
              this.showerror = true;
              this.error = err['error']['message'];
            }
            if(err['status'] == 404){
              this.showerror = true;
              this.error = err['error']['message'];
            }
            if(err['status'] == 500){
              this.showerror = true;
              this.error = err['error']['message'];
            }
          }
        )
      }
      
    
  };

  //Checks if the data is ok before sending a request
  ValidData(newp : kidprofile){
    if(newp.name.trim() == "" || newp.name == null || newp.name == undefined){
      this.error = "You need to fill the name space!!";
      this.showerror = true;
      return false;
    }
    if(newp.user_name.trim() == "" || newp.user_name == null || newp.user_name == undefined){
      this.error = "You need to fill the user name space!!";
      this.showerror = true;
      return false;
    }
    if(newp.pin.trim() == "" || newp.pin == null || newp.pin == undefined){
      this.error = "You need to fill the pin code name space!!";
      this.showerror = true;
      return false;
    }
    if(newp.age.trim() == "" || newp.age == null || newp.age == undefined){
      this.error = "You need to fill the age space!!";
      this.showerror = true;
      return false;
    }
    if(!Number.isInteger(parseInt(newp.age))){
      this.error = "You need to fill the age space with a number!!";
      this.showerror = true;
      return false;
    }
    if(parseInt(newp.age) >= 18 || parseInt(newp.age) <= 0 ){
      this.error = "You need to enter a valid age!!";
      this.showerror = true;
      return false;

    }
    if(newp.pin.length < 6 || newp.pin.length > 6){
      this.error = "You need to enter a 6 digit pin!!";
      this.showerror = true;
      return false;
    }
    else{
      return true;
    }

  }

  //Checks if the token is valid - Guard
  checkSession(){
    var actualDate = new Date();
    var token = localStorage.getItem('token');
    console.log(token);
    if(token != null){
      var decodetoken =  jwt_decode(token);
      console.log(decodetoken);
      if(decodetoken['permission'][0] != 'create' || decodetoken['permission'][1] != 'edit' || decodetoken['permission'][2] != 'delete'){
        localStorage.removeItem('token');
        console.log('get');
        this.router.navigate(['/index']);  
      }
      if(decodetoken['created'] / 3 != actualDate.getHours()){
        console.log('created');
        localStorage.removeItem('token');
        this.router.navigate(['/index']);
      }
      if((decodetoken['expire'] / 5 - actualDate.getHours()) != 1){
        console.log('expire');
        localStorage.removeItem('token');
        this.router.navigate(['/index']);
      }
    }
    else
    {
      this.router.navigate(['/index']);
    }
  
  }

}
