import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KidsdashboardComponent } from './kidsdashboard.component';

describe('KidsdashboardComponent', () => {
  let component: KidsdashboardComponent;
  let fixture: ComponentFixture<KidsdashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KidsdashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KidsdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
