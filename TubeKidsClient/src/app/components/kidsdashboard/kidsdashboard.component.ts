import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-kidsdashboard',
  templateUrl: './kidsdashboard.component.html',
  styleUrls: ['./kidsdashboard.component.css']
})
export class KidsdashboardComponent implements OnInit {

 
  
  constructor(private router: Router) { }

  ngOnInit() {
    this.checkSession();
  }

  //Logout the user
  logout(){
    console.log("SI");
    localStorage.removeItem('token');
    this.router.navigate(['/index']);
  }

  //Checks if the token is valid - Guard
  checkSession(){
    var actualDate = new Date();
    var token = localStorage.getItem('token');
    console.log(token);
    if(token != null){
      var decodetoken =  jwt_decode(token);
      console.log(decodetoken);
      if(decodetoken['permission'][0] != 'get'){
        localStorage.removeItem('token');
        console.log('get');
        this.router.navigate(['/index']);  
      }
      if(decodetoken['created'] / 3 != actualDate.getHours()){
        console.log('created');
        localStorage.removeItem('token');
        this.router.navigate(['/index']);
      }
      console.log(decodetoken['expire'] / 5 - actualDate.getHours());
      if((decodetoken['expire'] / 5 - actualDate.getHours()) != 1){
        console.log('expire');
        localStorage.removeItem('token');
        this.router.navigate(['/index']);
      }
    }
    else
    {
      this.router.navigate(['/index']);
    }
    

  }

}
