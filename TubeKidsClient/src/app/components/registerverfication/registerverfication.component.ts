import { Component, OnInit } from '@angular/core';
import { UserserviceService } from '../../services/user/userservice.service';
import { ActivatedRoute, Router } from "@angular/router";
import { registercode } from '../../models/registercode';


@Component({
  selector: 'app-registerverfication',
  templateUrl: './registerverfication.component.html',
  styleUrls: ['./registerverfication.component.css']
})
export class RegisterverficationComponent implements OnInit {
 
  error : string = "";
  showerror: boolean = false;
  showmain: boolean = true;
  shownext : boolean = false;
  registercode : registercode = {
    registrationcode: ""
  }
  constructor(private userservice: UserserviceService, private router: Router) { }

  ngOnInit() {
  }

  ActivateRegister(){
    if(this.ValidData(this.registercode.registrationcode)){
      this.userservice.activateAccount(this.registercode)
      .subscribe(
        res =>{
          if(res != null){
            this.showmain = false;
            this.shownext = true;
            console.log(res);
          }
          else{
            console.log(res);
          }
        },
        err => {
          if(err['status']== 400){
              this.showerror = true;
              this.error = err['error']['message'];
          }
          if(err['status'] == 404){
            this.showerror = true;
            this.error = err['error']['message'];
          }
          if(err['status'] == 500){
            this.showerror = true;
            this.error = err['error']['message'];
          }
        }
      );
    }
  }

  BadData(err : any){
    this.showerror = true;
    this.error = err;
  }
  //validates data
  ValidData(code :string){
    if(code == null || code.trim() == "" || code == undefined){
      this.error = 'You need to enter a  register code';
      this.showerror = true;
      return false;
    }
    else{
      return true;
    }
  }
}
