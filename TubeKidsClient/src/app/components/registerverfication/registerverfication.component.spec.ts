import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterverficationComponent } from './registerverfication.component';

describe('RegisterverficationComponent', () => {
  let component: RegisterverficationComponent;
  let fixture: ComponentFixture<RegisterverficationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterverficationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterverficationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
