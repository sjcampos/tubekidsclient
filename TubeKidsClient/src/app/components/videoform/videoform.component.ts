import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { video } from "../../models/video";
import { newvideo } from '../../models/newvideo';
import { VideoserviceService } from "../../services/video/videoservice.service";
import * as jwt_decode from 'jwt-decode';
@Component({
  selector: 'app-videoform',
  templateUrl: './videoform.component.html',
  styleUrls: ['./videoform.component.css']
})
export class VideoformComponent implements OnInit {
  
  update: boolean = false;
  register: boolean = true;
  error: string = "";
  showerror : boolean = false;
  v : video ={
    _id: "",
    id_user:"",
    video_name:"",
    url:""
  };
  nv : newvideo ={
    id : "",
    id_user: "",
    video_name:"",
    url:""
  } ;
  
 token = localStorage.getItem('token');
constructor(private videoservice: VideoserviceService, private router: Router, private activedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.checkSession();
    const params = this.activedRoute.snapshot.params;
    console.log(params);
    if(params.id){
      this.update = true;
      this.register = false;
      this.videoservice.getVideoUpdate(params.id).subscribe(
        res => {
          console.log(res);
          this.v = res;
          console.log(this.v);
          
        },
        err => console.log(err)
      );
    }
  }

  //Updates a video
  UpdateVideo(){
      if(this.ValidData(this.v)){
        this.videoservice.updateVideo(this.v, this.v._id)
        .subscribe(
          res=>{
            if(res != null){
              this.router.navigate(['/parentsmain']);
            }
  
          },
          (err) =>  {
            if(err['status'] == 401){
              this.showerror = true;
              this.error = err['error']['message'];
            }
            if(err['status'] == 404){
              this.showerror = true;
              this.error = err['error']['message'];
            }
            if(err['status'] == 500){
              this.showerror = true;
              this.error = err['error']['message'];
            }
          }
        )
      }
      
    
  }

  //Registers a new video
  RegisterVideo(){
    var decodetoken =  jwt_decode(this.token); 
    this.v.id_user = decodetoken['userId'];   
      if(this.ValidData(this.v)){
        this.nv.id = this.v._id;
        this.nv.id_user = this.v.id_user;
        this.nv.url = this.v.url;
        this.nv.video_name = this.v.video_name;
        this.videoservice.registerVideo(this.nv)
        .subscribe(
          res=>{
            if(res != null){
              console.log(res);
              this.router.navigate(['/parentsmain']);
            }
  
          },
          (err) =>  {
            if(err['status'] == 400){
              this.showerror = true;
              this.error = err['error']['message'];
            }
            if(err['status'] == 401){
              this.showerror = true;
              this.error = err['error']['message'];
            }
            if(err['status'] == 404){
              this.showerror = true;
              this.error = err['error']['message'];
            }
            if(err['status'] == 500){
              this.showerror = true;
              this.error = err['error']['message'];
            }
          }
        )
      }
      
    

  }
//Deletes a video
  DeleteVideo(){
    this.videoservice.deleteVideo(this.v._id)
    .subscribe(
      res=>{
        if(res != null){
          this.router.navigate(['/parentsmain']);
        }

      },
      (err) =>  {
        if(err['status'] == 401){
          this.showerror = true;
          this.error = err['error']['message'];
        }
        if(err['status'] == 404){
          this.showerror = true;
          this.error = err['error']['message'];
        }
        if(err['status'] == 500){
          this.showerror = true;
          this.error = err['error']['message'];
        }
      }
    )
  }

  //Validates data before sending a request
  ValidData(newv : video){
    if(newv.video_name.trim() == "" || newv.video_name == null || newv.video_name == undefined){
      this.error = "You need to fill the video name space!!";
      this.showerror = true;
      return false;
    }
    if(newv.url.trim() == "" || newv.url == null || newv.url == undefined){
      this.error = "You need to fill the url video space!!";
      this.showerror = true;
      return false;
    }
    else{
      return true;
    }

  }
//Checks if the token is valid - Guard
  checkSession(){
    var actualDate = new Date();
    var token = localStorage.getItem('token');
    console.log(token);
    if(token != null){
      var decodetoken =  jwt_decode(token);
      console.log(decodetoken);
      if(decodetoken['permission'][0] != 'create' || decodetoken['permission'][1] != 'edit' || decodetoken['permission'][2] != 'delete'){
        localStorage.removeItem('token');
        console.log('get');
        this.router.navigate(['/index']);  
      }
      if(decodetoken['created'] / 3 != actualDate.getHours()){
        console.log('created');
        localStorage.removeItem('token');
        this.router.navigate(['/index']);
      }
      if((decodetoken['expire'] / 5 - actualDate.getHours()) != 1){
        console.log('expire');
        localStorage.removeItem('token');
        this.router.navigate(['/index']);
      }
    }
    else
    {
      this.router.navigate(['/index']);
    }
  
  }

}
