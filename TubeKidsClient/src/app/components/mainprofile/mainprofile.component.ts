import { Component, OnInit } from '@angular/core';
import { ProfileserviceService} from "../../services/profile/profileservice.service";
import { ActivatedRoute, Router } from "@angular/router";
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-mainprofile',
  templateUrl: './mainprofile.component.html',
  styleUrls: ['./mainprofile.component.css']
})
export class MainprofileComponent implements OnInit {
  token = localStorage.getItem('token');
  profiles : any = [];
  error: string = "";
  showerror : boolean = false;
  constructor(private profileservice: ProfileserviceService, private router: Router) { }

  ngOnInit() {
    this.checkSession();
    this.getProfiles();
  }


  //Gets all the profiles
  getProfiles(){
    var decodetoken =  jwt_decode(this.token);
    this.profileservice.getProfiles(decodetoken['userId']).subscribe(
      res => {
        console.log(res);
        this.profiles = res;
      },
      err => console.log(err)
    );

  }
  //Deletes a profile
  DeleteProfile(id :any){
    this.profileservice.deleteProfile(id)
    .subscribe(
      res=>{
        console.log(res);
        if(res != null){
          this.getProfiles();
        }

      },
      (err) =>  {
        if(err['status'] == 401){
          this.showerror = true;
          this.error = err['error']['message'];
        }
        if(err['status'] == 404){
          this.showerror = true;
          this.error = err['error']['message'];
        }
        if(err['status'] == 500){
          this.showerror = true;
          this.error = err['error']['message'];
        }
      }
    )
  }

  //Checks if the token is valid - Guard
  checkSession(){
    var actualDate = new Date();
    var token = localStorage.getItem('token');
    console.log(token);
    if(token != null){
      var decodetoken =  jwt_decode(token);
      console.log(decodetoken);
      if(decodetoken['permission'][0] != 'create' || decodetoken['permission'][1] != 'edit' || decodetoken['permission'][2] != 'delete'){
        localStorage.removeItem('token');
        console.log('get');
        this.router.navigate(['/index']);  
      }
      if(decodetoken['created'] / 3 != actualDate.getHours()){
        console.log('created');
        localStorage.removeItem('token');
        this.router.navigate(['/index']);
      }
      if((decodetoken['expire'] / 5 - actualDate.getHours()) != 1){
        console.log('expire');
        localStorage.removeItem('token');
        this.router.navigate(['/index']);
      }
    }
    else
    {
      this.router.navigate(['/index']);
    }
  
  }

}
