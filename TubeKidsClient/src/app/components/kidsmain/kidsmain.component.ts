import { Component, OnInit } from '@angular/core';
import { VideoserviceService } from "../../services/video/videoservice.service";
import * as jwt_decode from 'jwt-decode';
import { video } from '../../models/video';
import { isBuffer } from 'util';
import { ActivatedRoute, Router } from "@angular/router";


@Component({
  selector: 'app-kidsmain',
  templateUrl: './kidsmain.component.html',
  styleUrls: ['./kidsmain.component.css']
})
export class KidsmainComponent implements OnInit {
  token = localStorage.getItem('token');
  videos : any = [];
  error: string = "";
  novideos: boolean =false;
  url: string = "";
  pos: any = 0;
  constructor(private videoservice: VideoserviceService,private router: Router) { }

  ngOnInit() {
    this.checkSession();
    this.getVideos();
  }

  //Gets all the videos 
  getVideos(){
    var decodetoken =  jwt_decode(this.token);
    this.videoservice.getVideos(decodetoken['fatherId']).subscribe(
      res => {
        console.log(res);
        if(res != null){
          this.videos = res;
        }
        else{
          this.novideos = true;
          this.error = "Looks like you don't have videos to watch!!"
        }
        
      },
      err => {
        if(err['status'] == 401){
          this.novideos = true;
          this.error = "Looks like you don't have access";
        }
        if(err['status'] == 404){
          this.novideos = true;
          this.error = err['error']['message'];
        }
        if(err['status'] == 500){
          this.novideos = true;
          this.error = err['error']['message'];
        }
      }
    )
  }
//Gets the url of the video and shows it also gets the position of
//the video in the list
  ShowVideo(u :any){
    for (let x = 0; x <= this.videos.length; x++) {
      console.log(x);
      if(this.videos[x].url == u){
        this.url = u;
        this.pos = x;
      }
    }
  }

  //Show the next video using the position in the list
  NextVideo(){
    if(this.pos == (this.videos.length-1)){
      this.pos = 0;
      this.url = this.videos[this.pos].url;
      
    }
    else{
      if(this.pos >= 0){
        this.pos = this.pos + 1;
        this.url = this.videos[this.pos].url;
        
      }
    }
    
  }

  //Shows the previous video using the position in the list
  PreviousVideo(){
    console.log(this.pos);
    if(this.pos <= 0)
    {
      this.pos = this.videos.length-1;
      this.url = this.videos[this.pos].url;
    }
    else{
      if(this.pos > 0 ){
        this.pos = this.pos - 1;
        this.url = this.videos[this.pos].url;
        
      }
    }
    
  }

  //Checks if the token is valid - Guard
  checkSession(){
    var actualDate = new Date();
    var token = localStorage.getItem('token');
    console.log(token);
    if(token != null){
      var decodetoken =  jwt_decode(token);
      console.log(decodetoken);
      if(decodetoken['permission'][0] != 'get'){
        localStorage.removeItem('token');
        console.log('get');
        this.router.navigate(['/index']);  
      }
      if(decodetoken['created'] / 3 != actualDate.getHours()){
        console.log('created');
        localStorage.removeItem('token');
        this.router.navigate(['/index']);
      }
      if((decodetoken['expire'] / 5 - actualDate.getHours()) != 1){
        console.log('expire');
        localStorage.removeItem('token');
        this.router.navigate(['/index']);
      }
    }
    else
    {
      this.router.navigate(['/index']);
    }
    

  }
  
}


