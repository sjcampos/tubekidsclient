import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KidsmainComponent } from './kidsmain.component';

describe('KidsmainComponent', () => {
  let component: KidsmainComponent;
  let fixture: ComponentFixture<KidsmainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KidsmainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KidsmainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
