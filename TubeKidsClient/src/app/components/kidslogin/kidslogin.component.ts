import { Component, OnInit } from '@angular/core';
import { kidprofile } from "../../models/kidprofile";
import { ProfileserviceService } from "../../services/profile/profileservice.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-kidslogin',
  templateUrl: './kidslogin.component.html',
  styleUrls: ['./kidslogin.component.css']
})
export class KidsloginComponent implements OnInit {

  error: string = "";
  
  showerror : boolean = false;
  kidprofile: kidprofile ={ //esto hay que cambiarlo por un modelo de login igual que el user
    _id:"",
    id_father:"",
    name: "",
    user_name:"",
    pin: "",
    age:""
  };
  constructor(private profileservice: ProfileserviceService, private router: Router) { }

  ngOnInit() {
  }

  //Login the profiles  for the kids
  AuthData(){
    if(this.ValidData(this.kidprofile)){
      console.log('valido');
      this.profileservice.checkProfile(this.kidprofile)
      .subscribe(
        res=>{
          console.log(res);
          localStorage.setItem('token', res['token']);
          this.router.navigate(['/kidsmain']);
        },
        (err) =>  {
          if(err['status']== 400){
              this.showerror = true;
              this.error = err['error']['message'];
          }
          if(err['status'] == 404){
            this.showerror = true;
            this.error = err['error']['message'];
          }
          if(err['status'] == 500){
            this.showerror = true;
            this.error = err['error']['message'];
          }
        }
      );
    }
  }



  //Validates the profile data before sending a request
  ValidData(profile : kidprofile){
    if(profile.user_name == "" || profile.pin == ""  || profile.user_name == null || profile.pin == null || profile.user_name == undefined ||
    profile.pin == undefined || profile.user_name.trim() == "" || profile.pin.trim() == ""){
      this.showerror = true;
      this.error = "You need to complete all the fields!!";
      return false;
    }
    else{
      return true;
    }

  }

}
