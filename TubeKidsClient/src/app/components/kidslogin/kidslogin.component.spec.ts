import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KidsloginComponent } from './kidslogin.component';

describe('KidsloginComponent', () => {
  let component: KidsloginComponent;
  let fixture: ComponentFixture<KidsloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KidsloginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KidsloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
