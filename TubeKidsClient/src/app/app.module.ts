import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { HttpConfigInterceptor} from './interceptor/httpconfig.interceptor';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './components/index/index.component';
import { ParentsloginComponent } from './components/parentslogin/parentslogin.component';
import { KidsloginComponent } from './components/kidslogin/kidslogin.component';
import { RegisterformComponent } from './components/registerform/registerform.component';
import { ParentsmainComponent } from './components/parentsmain/parentsmain.component';
import { KidsmainComponent } from './components/kidsmain/kidsmain.component';
import { VideoformComponent } from './components/videoform/videoform.component';
import { FormsModule } from '@angular/forms';
import { SmsverficationComponent } from './components/smsverfication/smsverfication.component';
import { RegisterverficationComponent } from './components/registerverfication/registerverfication.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { KidsdashboardComponent } from './components/kidsdashboard/kidsdashboard.component';
import { IndexdashboardComponent } from './components/indexdashboard/indexdashboard.component';
import { ProfileformComponent } from './components/profileform/profileform.component';
import { MainprofileComponent } from './components/mainprofile/mainprofile.component';
import { SafeurlPipe } from './pipes/safeurl.pipe';
@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    ParentsloginComponent,
    KidsloginComponent,
    RegisterformComponent,
    ParentsmainComponent,
    KidsmainComponent,
    VideoformComponent,
    SmsverficationComponent,
    RegisterverficationComponent,
    DashboardComponent,
    KidsdashboardComponent,
    IndexdashboardComponent,
    ProfileformComponent,
    MainprofileComponent,
    SafeurlPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    HttpClientModule,
    FormsModule,
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
