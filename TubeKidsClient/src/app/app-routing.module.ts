import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './components/index/index.component';
import { KidsloginComponent } from './components/kidslogin/kidslogin.component';
import { ParentsloginComponent } from './components/parentslogin/parentslogin.component';
import { SmsverficationComponent } from './components/smsverfication/smsverfication.component';
import { ParentsmainComponent } from './components/parentsmain/parentsmain.component';
import { KidsmainComponent } from './components/kidsmain/kidsmain.component';
import { RegisterformComponent } from './components/registerform/registerform.component';
import { RegisterverficationComponent } from './components/registerverfication/registerverfication.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { IndexdashboardComponent } from './components/indexdashboard/indexdashboard.component';
import { VideoformComponent } from './components/videoform/videoform.component';
import { ProfileformComponent } from './components/profileform/profileform.component';
import { MainprofileComponent } from './components/mainprofile/mainprofile.component';
import { KidsdashboardComponent } from './components/kidsdashboard/kidsdashboard.component';



const routes: Routes = [
  {path:'', redirectTo:'index', pathMatch:'full'},
  {path:'index', component:IndexComponent},
  {path:'register', component:RegisterformComponent},
  {path:'kidslogin', component:KidsloginComponent},
  {path:'parentslogin', component:ParentsloginComponent},
  {path:'registerverification', component:RegisterverficationComponent},
  {path:'smsverification', component:SmsverficationComponent},
  {path:'parentsmain', component:ParentsmainComponent},
  {path:'kidsmain', component: KidsmainComponent},
  {path:'dashboard', component: DashboardComponent},
  {path:'indexdashboard', component:IndexdashboardComponent},
  {path:'kidsdashboard', component:KidsdashboardComponent},
  {path: 'videoform/add', component:VideoformComponent},
  {path:'profileform', component:ProfileformComponent},
  {path:'mainprofile', component:MainprofileComponent},
  {path:'videoform/edit/:id', component:VideoformComponent},
  {path: 'profileform/edit/:id', component:ProfileformComponent},
 
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
