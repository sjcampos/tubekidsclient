export interface user{
    id: string;
    name: string;
    last_name:string;
    country:string;
    phone: string;
    birthday: string;
    email: string;
    password: string;
    repass: string;
}