import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { video } from "../../models/video";
import { newvideo } from "../../models/newvideo";

@Injectable({
  providedIn: 'root'
})
export class VideoserviceService {

  constructor(protected http: HttpClient) { }
    //Registers a new video
    registerVideo(newvideo :newvideo){
      return this.http.post('http://localhost:4000/api/videos', newvideo);
    }

    //Gets all the videos for a specific user or profile
    getVideos(id_user: any){
      return this.http.get(`http://localhost:4000/api/videos/${id_user}`);
    }
    //Gets a video for updating
    getVideoUpdate(id : any){
      return this.http.get<video>(`http://localhost:4000/api/videos/videodata/${id}`);
    }

    updateVideo(video: video, id: any){
      return this.http.put(`http://localhost:4000/api/videos/${id}`, video);
    }

    deleteVideo(id: any){
      return this.http.delete(`http://localhost:4000/api/videos/${id}`);
    }
  
}
