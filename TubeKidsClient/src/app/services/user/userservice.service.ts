import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { user } from '../../models/user';
import { userlogin } from '../../models/userlogin';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  constructor(protected http: HttpClient) { }

  //Auth the user credentials and sends the sms
  checkUSer(userlogin: userlogin){
    return this.http.post('http://localhost:4000/api/authentic', userlogin);
  }
  //Checks the sms code and gets the jwt
  checkmessage(verificationcode: any){
    return this.http.post('http://localhost:4000/api/session',verificationcode);
  }

  //Registers a new user
  registerUser(newuser :user){
    return this.http.post('http://localhost:4000/api/users', newuser);
  }

  //Activates users new´s acccount´s
  activateAccount(registrationcode :any){
    return this.http.put('http://localhost:4000/api/activenew', registrationcode);
  }

}
