import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { kidprofile } from '../../models/kidprofile';
import { newkidprofile } from "../../models/newkidprofile";

@Injectable({
  providedIn: 'root'
})
export class ProfileserviceService {

  constructor(protected http: HttpClient) { }

    //Auth the profile credentials and gets the jwt
    checkProfile(kidprofile: kidprofile){
      return this.http.post('http://localhost:4000/api/restrictedsession', kidprofile);
    }

      //Registers a new profile
    registerProfile(newprofile :newkidprofile){
      return this.http.post('http://localhost:4000/api/profiles', newprofile);
    }

    //gets all the profiles
    getProfiles(id_father: any){
      return this.http.get(`http://localhost:4000/api/profiles/${id_father}`);
    }

    //Gets the profile data for updating
    getProfileUpdate(id:any){
      return this.http.get<kidprofile>(`http://localhost:4000/api/profiles/profiledata/${id}`);
    }

    updateProfile(profile: kidprofile, id: any){
      return this.http.put(`http://localhost:4000/api/profiles/${id}`, profile);
    }

    deleteProfile(id: any){
      return this.http.delete(`http://localhost:4000/api/profiles/${id}`);
    }
}
