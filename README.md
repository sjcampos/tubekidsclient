**Tubekids Client App**

This is the backend server for the tubekids project in the web programming class 2.

**It was built with  🛠️**

[Angular](https://angular.io/) - As framework

[Bootstrap](https://getbootstrap.com/) - CSS framework

**Author ✒️**

Sebastián Campos